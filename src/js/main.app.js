import React from 'react';
import ReactDOM from 'react-dom';
import News from "./components/news/News";
import Gallery from "./components/gallery/Gallery";
import Subscribe from "./components/subscribe form/Subscribe";
import 'whatwg-fetch';
import {lazyBackground, lazyLoad} from "./components/lazyLoadFunction";

ReactDOM.render(
    <News/>,
    document.getElementById('news')
);

ReactDOM.render(
    <Gallery/>,
    document.getElementById('company-gallery')
);

ReactDOM.render(
    <Subscribe/>,
    document.getElementById('subscribe-form')
);

document.addEventListener("DOMContentLoaded", function () {
    lazyLoad('lazy');
    lazyBackground('lazy-background');
});

