import React from 'react';
import NewsItem from "./NewsItem";
import Loader from "../Loader";

class News extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            newsCounter: 6,
            isLoaded: false,
            news: []
        }
    }

    loadMore = (newsLength) => {
        this.setState({
            newsCounter: newsLength + 6
        });
    };

    componentDidMount() {
        fetch("https://jsonplaceholder.typicode.com/posts").then(
            response => {
                return response.json()
            })
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        news: result
                    })
                }
            )
    }

    render() {
        let {isLoaded, news, newsCounter} = this.state;
        let newsArray = news.slice(0, newsCounter);
        return (
            <React.Fragment>
                <div className={'wrapper'}>
                    <div className={'news-wrapper' + (!isLoaded ? ' loader' : '')}>
                        {!isLoaded ? <Loader/> : ''}
                        {newsArray.map((item, index) => {
                            return (
                                <NewsItem key={index} news={item}/>
                            )
                        })}
                    </div>
                </div>
                <div className={'load-more-wrapper'}>
                    <a href={'#'}
                       className={'load-more'}
                       onClick={e => {
                           e.preventDefault();
                           this.loadMore(newsCounter);
                       }}>More News</a>
                </div>
            </React.Fragment>
        )
    }
}

export default News;