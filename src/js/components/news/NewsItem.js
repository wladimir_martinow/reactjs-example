import React from 'react';
import Loader from "../Loader";
import {lazyLoad} from "../lazyLoadFunction";

class NewsItem extends React.Component {
    state = {
        isLoaded: false
    };

    componentDidMount() {
        lazyLoad('lazy');
    }

    render() {
        let {news} = this.props;
        let {isLoaded} = this.state;
        return (
            <div className={'news-item'}>
                <div className={'news-img-holder' + (!isLoaded ? ' loader' : '')}>
                    {!isLoaded ? <Loader/> : ''}
                    <img
                        src={''}
                        className={'lazy'}
                        data-srcset={'https://picsum.photos/432/288?random=' + news.id}
                        data-src={'https://picsum.photos/432/288?random=' + news.id}
                        alt={news.title}
                        onLoad={() => {
                        this.setState({
                            isLoaded: true
                        });
                    }}/>
                </div>
                <div className="news-text-holder">
                    <h3>{news.title}</h3>
                    <span>{news.body}</span>
                    <a href={'http://www.google.com/'} target={'_blank'} className={'read-me'}><i className="fas fa-align-right"></i> Read me</a>
                </div>
            </div>
        )
    }
}

export default NewsItem;