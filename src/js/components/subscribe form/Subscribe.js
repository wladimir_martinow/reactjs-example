import React from 'react';

class Subscribe extends React.Component {
    state = {
        email: '',
        validated: true
    };

    inputChange = (value) => {
        this.setState({
            email: value
        })
    };

    validation = () => {
        const expression = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        console.log(expression.test((this.state.email).toLowerCase()));
        return expression.test((this.state.email).toLowerCase());
    };

    render() {
        let {email, validated} = this.state;
        return (
            <React.Fragment>
                <form onSubmit={e => {
                    e.preventDefault();
                    let validated = this.validation();
                    this.setState({
                        validated: validated
                    });
                }}
                >
                    <input
                        value={email ? email : ''}
                        type={'email'}
                        name={'email'}
                        placeholder={'Your email'}
                        onChange={e => {
                            this.inputChange(e.target.value);
                        }}
                    />
                    <input
                        type={'submit'}
                        value={'Subscribe'}
                    />
                </form>
                {!validated ? <span className={'error'}>Your email is not valid!</span> : ''}
            </React.Fragment>
        )
    }
}

export default Subscribe;