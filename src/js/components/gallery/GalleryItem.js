import React from 'react';
import Loader from "../Loader";

class GalleryItem extends React.Component {
    state = {
        isLoaded: false
    };

    render() {
        let {items} = this.props;
        let {isLoaded} = this.state;
        return (
            <div className={'gallery-item'}>
                {items.map((item, index) => {
                    return (
                        <div key={index}
                            className={'gallery-item-part' + (index === 0 ? ' big-image' : '') + (!isLoaded ? ' loader' : '')}>
                            {!isLoaded ? <Loader/> : ''}
                            <img src={index === 0 ? item.url : item.thumbnailUrl} alt={item.title} onLoad={() => {
                                this.setState({
                                    isLoaded: true
                                });
                            }}/>
                        </div>
                    )
                })}
            </div>
        )
    }
}

export default GalleryItem;