import React from "react";
import Slider from "react-slick";
import GalleryItem from "./GalleryItem";

class SimpleSlider extends React.Component {
    render() {
        let {items, lastItem, increase} = this.props;
        let settings = {
            autoplay:true,
            autoplaySpeed: 2500,
            lazyLoad: true,
            dots: false,
            infinite: false,
            speed: 600,
            slidesToShow: 6,
            slidesToScroll: 3,
            beforeChange: (current) => {
                if(current + 6 > lastItem){
                    increase();
                }
            }
        };

        return (
            <Slider  {...settings}>
                {items.map((item, index) => {
                    return(
                        <GalleryItem key={index}  items={item}/>
                    )
                })}
            </Slider>
        );
    }
}

export default SimpleSlider;