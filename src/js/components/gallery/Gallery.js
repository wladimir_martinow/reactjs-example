import React from 'react';
import SimpleSlider from "./Slider";

class Gallery extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            currentIndex: 0,
            itemsCount: 3,
            isLoaded: false,
            lastItem: 30,
            galleryItems: []
        }
    }

    spliceItemCollection = (collection, start, perCount) => {
        let newCollection = [];
        for (let i = start; i < collection.length; i += perCount) {
            let singleGalleryItem = collection.slice(i, i + perCount);
            newCollection.push(singleGalleryItem);
        }
        return newCollection;
    };

    componentDidMount() {
        fetch("https://jsonplaceholder.typicode.com/photos").then(
            response => {
                return response.json()
            })
            .then(
                (result) => {
                    let tripleItem = this.spliceItemCollection(result, this.state.currentIndex, this.state.itemsCount);
                    this.setState({
                        galleryItems: tripleItem
                    });
                }
            )
    }

    increaseLastItem = () => {
        let lastItem = this.state.lastItem;
        if(lastItem < this.state.galleryItems.length){
            this.setState({
                lastItem: lastItem + 30
            });
        }
    };

    render() {
        let {galleryItems, lastItem} = this.state;
        let items = galleryItems.slice(0, lastItem);
        return (
            <div className="company-gallery-items">
                <SimpleSlider lastItem={lastItem} increase={this.increaseLastItem} items={items}/>
            </div>
        )
    }
}

export default Gallery;